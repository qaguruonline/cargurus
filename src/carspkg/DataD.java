package carspkg;

import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;


/* Author qa guru */
public class DataD {

	@DataProvider(name = "data-provider")
	public Object[][] dataProviderMethod() {
		return new Object[][] { { "DFW", "ORD" }, { "ATL","DFW" }, {"LAX", "ATL"} };
	}


	WebDriver driver;
	public static String testUrl = "http://www.cargurus.com/es";

	@BeforeMethod
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "/Users/qatester/Documents/seleniumdrivers/chromedriver");
		driver = new ChromeDriver();

	}

	
	@Test(dataProvider = "data-provider")
	public void testflights(String origin, String dest) {
		
		driver.get("http://www.aa.com");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//*[@id='jq-flightStatus']")).click();
		driver.findElement(By.id("flightStatusOriginAirport")).sendKeys(origin);
		driver.findElement(By.id("flightStatusDestinationAirport")).sendKeys(dest);
		//System.out.println("Data is: " + data);
	}
}
