package carspkg;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class carsTestes {

	WebDriver driver;
	public static String testUrl = "http://www.cargurus.com/es";

	@BeforeMethod
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "/Users/qatester/Documents/seleniumdrivers/chromedriver");
		driver = new ChromeDriver();

	}

	@Test(priority = 1, enabled = true)
	public void verifyPagetitle() {
		driver.get(testUrl);
		String actualTitle = driver.getTitle();
		String expectedTitle = "Autos usados, autos nuevos, evaluaciones, fotos y opiniones - CarGurus";

		System.out.println(actualTitle);
		System.out.println(expectedTitle);

		Assert.assertEquals(actualTitle, expectedTitle);

	}

	@Test(priority = 2, enabled = true)
	@Parameters({"zipcode1"})
	public void CPObyZipCode(String zipcode) {
		driver.get(testUrl);
		driver.findElement(By.id("heroSearch-tab-2")).click(); // clicking Preowned Zipcode
		WebElement cpoZip = driver.findElement(By.id("dealFinderZipCPOId"));
		cpoZip.sendKeys(zipcode); // enter data in zipcode textbox
		driver.findElement(By.id("dealFinderFormCPO_0")).sendKeys(Keys.ENTER); // clicking search button

		String actualTitle = driver.getTitle();
		Assert.assertTrue(actualTitle.contains("Carrollton"));

	}

	@Test(priority = 3, enabled = true)
	@Parameters({"zipcode2"})
	public void verifyH1CPOByZipCode(String zipcode) {
		driver.get(testUrl);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.id("heroSearch-tab-2")).click(); // clicking Preowned Zipcode
		driver.findElement(By.id("dealFinderZipCPOId")).sendKeys(zipcode); // enter data in zipcode textbox
		driver.findElement(By.id("dealFinderFormCPO_0")).sendKeys(Keys.ENTER); // clicking search button
		String heading = driver.findElement(By.xpath("//*[@id=\"listingResultsContentBody\"]/div[1]/h1")).getText();
		Assert.assertTrue(heading.contains("Plano, TX"));

	}

	@Test
	public void verifyHeadingMakemodelByZIPCode() {
		driver.get(testUrl);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.id("heroSearch-tab-2")).click(); // clicking Preowned Zipcode
		Select makerSelect = new Select(driver.findElement(By.id("carPickerCpo_makerSelect")));
		makerSelect.selectByVisibleText("Acura");
		Select modelSelect = new Select(driver.findElement(By.id("carPickerCpo_modelSelect")));
		modelSelect.selectByVisibleText("MDX");
		driver.findElement(By.id("dealFinderZipCPOId")).sendKeys("75007"); // enter data in zipcode textbox
		driver.findElement(By.id("dealFinderFormCPO_0")).sendKeys(Keys.ENTER);
		String heading = driver.findElement(By.xpath("//*[@id=\"listingResultsContentBody\"]/div[1]/h1/em[1]"))
				.getText();
		Assert.assertTrue(heading.contains("Acura"));

	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
